from setuptools import setup, find_packages

setup(name="jeepney",
    license="License :: OSI Approved :: MIT License",
    packages=find_packages(),
    include_package_data = True,
    zip_safe = False,
    install_requires = [
        "enum34",
        "trollius",
        "six",
        "typing",
    ],
)
