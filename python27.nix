{ pkgs ? import <nixpkgs> {} }:

let aPython = pkgs.python27.override {
  packageOverrides = self: super: rec {
    callPackage = pkgs.newScope self;
    aiogevent = callPackage ./nix/aiogevent.nix { };
  };
};

pyPkgs = aPython.pkgs;

in

pyPkgs.buildPythonApplication {
  name = "jeepney-dev";
  buildInputs = with pyPkgs; [ trollius typing testpath aiogevent six gevent pytest pylint tornado ];
  shellHook = ''
    PYTHONPATH=$PYTHONPATH:$(pwd)
  '';
}
