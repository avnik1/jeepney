{ lib, fetchFromGitHub, buildPythonPackage, gevent, trollius }:

buildPythonPackage rec {
  pname = "aiogevent";
  version = "2752c68";
  name = "${pname}-${version}";

  src = fetchFromGitHub {
    owner = "2mf";
    repo = pname;
    rev = version;
    sha256 = "1zcgx7g776s117ml3mv0vxnl09fg6zrxjiwllkxvpv6vvvp3r85i";
  };

  propagatedBuildInputs = [ gevent trollius ];
  doCheck = false;
  
  meta = {
    homepage = https://github.com/2mf/aiogevent;
    description = "aio gevent adapter";
    license = lib.licenses.lgpl2Plus;
    maintainers = with lib.maintainers; [ avnik ];
  };
}
