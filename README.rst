This is backport of jeepney to python 2.7 and `trollius`.
Only `blocking` and `asyncio` backends proved to be workable.
For python 3.7+ use `upstream repo <https://gitlab.com/takluyver/jeepney>`__

This is a low-level, pure Python DBus protocol client. It has an `I/O-free
<https://sans-io.readthedocs.io/>`__ core, and integration modules for different
event loops.

DBus is an inter-process communication system, mainly used in Linux.

`Jeepney docs on Readthedocs <https://jeepney.readthedocs.io/en/latest/>`__

This project is experimental, and there are a
number of `more mature Python DBus bindings <https://www.freedesktop.org/wiki/Software/DBusBindings/#python>`__.
